package com.tsystems.javaschool.tasks.pyramid;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */

   public int[][] buildPyramid(List<Integer> inputNumbers) {

       if (inputNumbers.contains(null)) throw new CannotBuildPyramidException();

       int rowsNumber = 0;
       int lenght = inputNumbers.size();
       while(lenght > 0) lenght -= ++rowsNumber;
       if (lenght != 0) throw new CannotBuildPyramidException();

       Collections.sort(inputNumbers);

       int colsNumber = rowsNumber * 2 - 1;
       int[][] result = new int[rowsNumber][colsNumber];

        int bitMap = 1 << rowsNumber;
        int listIterator = 0;
        for (int i = 0; i < rowsNumber; i++) {
            for (int j = 1; j <= colsNumber; j++) {
                if ((bitMap & (1L << j)) != 0) {
                    result[i][j - 1] = inputNumbers.get(listIterator++);
                }
            }
            bitMap = bitMap << 1;
            bitMap = bitMap ^ (1 << (rowsNumber - i - 1));
        }

        System.out.println(inputNumbers);
        for (int[] row : result) {
            System.out.println(Arrays.toString(row));
        }

        return result;
    }
}
