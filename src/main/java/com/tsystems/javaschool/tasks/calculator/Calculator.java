package com.tsystems.javaschool.tasks.calculator;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Stack;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 3) * 4 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */

    public String evaluate(String statement) {
        if ((statement == null) || statement.isEmpty()) return null;
        ArrayList<String> postfix;
        try {
            postfix = getPostfix(statement);
        } catch (ParseException e) {
            return null;
        }
        Stack<Double> workStack = new Stack<>();
        for (String item : postfix) {
            if (isDouble(item)) {
                workStack.push(Double.parseDouble(item));
            } else {
                double result = 0;
                double num2 = workStack.pop();
                double num1 = workStack.pop();

                switch (item) {
                    case "+":
                        result = num1 + num2;
                        break;
                    case "-":
                        result = num1 - num2;
                        break;
                    case "*":
                        result = num1 * num2;
                        break;
                    case "/":
                        result = num1 / num2;
                        break;
                }
                if (Double.isFinite(result)) workStack.push(result);
                else return null;
            }
        }

        return new DecimalFormat("#.####", new DecimalFormatSymbols(new Locale("en"))).format(workStack.pop());
    }

    private ArrayList<String> getPostfix (String statement) throws ParseException {
        Stack<Character> stack = new Stack<>();
        ArrayList<String> result = new ArrayList<>();
        StringBuilder operand = new StringBuilder();
        boolean lastOperator = true;

        for (int i = 0; i < statement.length(); i++) {
            char ch = statement.charAt(i);
            if (Character.isDigit(ch) || ch == '.') {
                operand.append(ch);
                if (ch == '.') {
                    if (lastOperator) throw new ParseException("Two operators in row", 1);
                    lastOperator = true;
                } else {
                    lastOperator = false;
                }
            } else {
                if (operand.length() != 0) {
                    result.add(operand.toString());
                    operand.setLength(0);
                }
                switch (ch) {
                    case ' ':
                        break;
                    case '+':
                    case '-':
                        if (lastOperator) throw new ParseException("Two operators in row", 1);
                        lastOperator = true;
                        while (!stack.isEmpty()) {
                            char topOperand = stack.pop();
                            if (topOperand == '(') {
                                stack.push(topOperand);
                                break;
                            } else {
                                result.add(String.valueOf(topOperand));
                            }
                        }
                        stack.push(ch);
                        break;
                    case '*':
                    case '/':
                        if (lastOperator) throw new ParseException("Two operators in row", 1);
                        lastOperator = true;
                        while (!stack.isEmpty()) {
                            char topOperand = stack.pop();
                            if (topOperand == '(') {
                                stack.push(topOperand);
                                break;
                            } else {
                                if ((topOperand == '-') || (topOperand == '+')) {
                                    stack.push(topOperand);
                                    break;
                                } else {
                                    result.add(String.valueOf(topOperand));
                                }
                            }
                        }
                        stack.push(ch);
                        break;
                    case '(':
                        stack.push(ch);
                        break;
                    case ')':
                        while (true) {
                            if (stack.isEmpty()) throw new ParseException("No open parenthesis", 2);
                            char topCh = stack.pop();
                            if (topCh == '(') break;
                            else result.add(String.valueOf(topCh));
                        }
                        break;
                    default:
                        throw new ParseException("Got wrong character", 3);
                }
            }
        }

        if (operand.length() != 0) {
            result.add(operand.toString());
        }

        while (!stack.isEmpty()) {
            if (stack.peek() == '(') throw new ParseException("Who need open parenthesis? I got one left!", 4);
            result.add(String.valueOf(stack.pop()));
        }
        return result;
    }

    private boolean isDouble (String guess) {
        try {
            Double.parseDouble(guess);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }
}